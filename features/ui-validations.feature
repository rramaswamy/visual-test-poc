@TeacherSiteVisualTesting
Feature: Applitools Test Demo

	
@TeacherLogin @UI_1 
Scenario: Applitools test demo on teacher site
   When verify user is on teacher site
   Then verify resources and tools tab is present
   Then verify teacher logo is present by applitool
   
@Teacherresourceandtools @UI_2 
Scenario: Applitools test demo on links
   When verify user is on teacher site
   Then verify resources and tools tab is present
   Then verify teacher logo is present by applitool
   Then verify links on resource and tools tab
   Then verify links on books and auther tab

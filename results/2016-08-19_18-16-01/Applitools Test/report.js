$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/ui-validations.feature");
formatter.feature({
  "id": "applitools-test-demo",
  "tags": [
    {
      "name": "@TeacherSiteVisualTesting",
      "line": 1
    }
  ],
  "description": "",
  "name": "Applitools Test Demo",
  "keyword": "Feature",
  "line": 2
});
formatter.before({
  "duration": 10680393000,
  "status": "passed"
});
formatter.scenario({
  "id": "applitools-test-demo;applitools-test-demo-on-links",
  "tags": [
    {
      "name": "@Teacherresourceandtools",
      "line": 11
    },
    {
      "name": "@UI_2",
      "line": 11
    }
  ],
  "description": "",
  "name": "Applitools test demo on links",
  "keyword": "Scenario",
  "line": 12,
  "type": "scenario"
});
formatter.step({
  "name": "verify user is on teacher site",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "verify resources and tools tab is present",
  "keyword": "Then ",
  "line": 14
});
formatter.step({
  "name": "verify teacher logo is present by applitool",
  "keyword": "Then ",
  "line": 15
});
formatter.step({
  "name": "verify links on resource and tools tab",
  "keyword": "Then ",
  "line": 16
});
formatter.step({
  "name": "verify links on books and auther tab",
  "keyword": "Then ",
  "line": 17
});
formatter.match({
  "location": "UIValidationsStepdefs.verify_teacher_site_page()"
});
formatter.result({
  "duration": 270136000,
  "status": "passed"
});
formatter.match({
  "location": "UIValidationsStepdefs.verify_resources_tools_tab()"
});
formatter.result({
  "duration": 101640000,
  "status": "passed"
});
formatter.match({
  "location": "UIValidationsStepdefs.verify_teacher_logo()"
});
formatter.result({
  "duration": 7171215000,
  "status": "passed"
});
formatter.match({
  "location": "UIValidationsStepdefs.verify_links_on_resource_tools_tab()"
});
formatter.result({
  "duration": 5396914000,
  "status": "passed"
});
formatter.match({
  "location": "UIValidationsStepdefs.verify_links_on_books_and_auther_tab()"
});
formatter.result({
  "duration": 9473101000,
  "status": "passed"
});
formatter.after({
  "duration": 1296123000,
  "status": "passed"
});
});
package com.applitools.Utilities;

import com.applitools.eyes.Eyes;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.TestResults;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SampleCodeSnip {

    public static void main(String[] args) throws Exception {
        WebDriver driver = new FirefoxDriver();

        Eyes eyes = new Eyes();
        // This is your api key, make sure you use it in all your tests.
        eyes.setApiKey("9tHA7E3iDCCYDmKq9DGVL602Ei0Ahazd4BISa962UCU110");

        try {
            // Start visual testing with browser viewport set to 1024x768.
            // Make sure to use the returned driver from this point on.
            driver = eyes.open(driver, "Applitools", "Test Web Page", new RectangleSize(1000, 600));

            driver.get("http://applitools.com");

            // Visual validation point #1
            eyes.checkWindow("Main Page");

            driver.findElement(By.cssSelector(".features")).click();

            // Visual validation point #2
            eyes.checkWindow("Features page");

            // End visual testing. Validate visual correctness.
            boolean[] stepsResults = close(eyes, "/Users/is-4583/Documents/digital-platform-test-automation_Applitools/DPTest/results", "9tHA7E3iDCCYDmKq9DGVL602Ei0Ahazd4BISa962UCU110");
        } finally {
            // Abort test in case of an unexpected error.
            eyes.abortIfNotClosed();
            driver.quit();
        }
    }

    public static boolean[] close(Eyes eyes, String diffsFolder, String viewKey) throws Exception {
        TestResults results = eyes.close(false);

        boolean[] stepStates = calculateStepResults(eyes, results, viewKey);
        saveDiffs(eyes, results, diffsFolder, viewKey, stepStates);
        return stepStates;
    }

    public static boolean[] calculateStepResults(Eyes eyes, TestResults results, String viewKey) throws Exception {
        return stepResults(getSessionId(results), getBatchId(results), viewKey);
    }

    public static void saveDiffs(Eyes eyes, TestResults results, String destinationFolder, String viewKey, boolean[] stepsState) throws IOException {
        ArrayList<String> urls = getDiffUrls(results, viewKey, stepsState);
        saveImages(urls, destinationFolder);
    }

    public static void saveImages(ArrayList<String> urls, String destination) throws IOException {
        int step = 1;
        String destFormat = "%s//step_%s_diffs.png";
        for (String url : urls) {
            saveImage(url, String.format(destFormat, destination, step));
            ++step;
        }
    }

    public static void saveImage(String imageUrl, String destinationFile) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationFile);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    public static ArrayList<String> getDiffUrls(TestResults results, String viewKey, boolean[] stepsState) {
        ArrayList<String> urls = new ArrayList<String>(results.getSteps());
        String sessionId = getSessionId(results);
        String batchId = getBatchId(results);
        String urlTemplate = "https://eyes.applitools.com/api/sessions/%s/steps/%s/diff?ApiKey=%s";
        for (int step = 0; step < results.getSteps(); ++step) {
            if (stepsState[step] == false) {
                urls.add(String.format(urlTemplate, sessionId, step + 1, viewKey));
            }
        }
        return urls;
    }

    public static String getSessionId(TestResults results) {
        Pattern p = Pattern.compile("^https://eyes.applitools.com/app/sessions/\\d+/(?<sessionId>\\d+).*$");
        Matcher matcher = p.matcher(results.getUrl());
        if (!matcher.find()) return null; //Something wrong with our url
        return matcher.group("sessionId");
    }

    public static String getBatchId(TestResults results) {
        Pattern p = Pattern.compile("^https://eyes.applitools.com/app/sessions/(?<batchId>\\d+)/\\d+.*$");
        Matcher matcher = p.matcher(results.getUrl());
        if (!matcher.find()) return null; //Something wrong with our url
        return matcher.group("batchId");
    }

    public static boolean[] stepResults(String sessionId, String batchId, String viewKey) throws Exception {
        String url = String.format("https://eyes.applitools.com/api/sessions/batches/%s/%s/?ApiKey=%s&format=json", batchId, sessionId, viewKey);
        String json = readJsonStringFromUrl(url);

        JSONObject obj = new JSONObject(json);
        JSONArray expected = obj.getJSONArray("expectedAppOutput");
        JSONArray actual = obj.getJSONArray("actualAppOutput");

        int steps = expected.length();
        boolean[] retStepResults = new boolean[steps];

        for (int i = 0; i < steps; i++) {
            retStepResults[i] = (actual.get(i) == JSONObject.NULL) || actual.getJSONObject(i).getBoolean("isMatching") == true;
        }

        return retStepResults;
    }

    public static String readJsonStringFromUrl(String url) throws Exception {
        SSLSocketFactory defaultSSLSocketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();
        HttpsURLConnection.setDefaultSSLSocketFactory(new sun.security.ssl.SSLSocketFactoryImpl());
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            return readAll(rd);
        } finally {
            is.close();
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
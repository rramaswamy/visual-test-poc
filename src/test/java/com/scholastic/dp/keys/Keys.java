
package com.scholastic.dp.keys;

public interface Keys {

	public interface TeacherPageLocators {
		String RESOURCES_TOOLS_TAB = "resources.tools.tab";
		String TEACHER_LOGO = "teacher.logo";
		String BOOKS_AUTHER = "books.auther.tab";
}
}

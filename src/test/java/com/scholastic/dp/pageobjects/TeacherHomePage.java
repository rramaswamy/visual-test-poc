package com.scholastic.dp.pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.log4testng.Logger;

import com.applitools.Utilities.ApplitoolsUtils;
import com.scholastic.dp.keys.Keys.TeacherPageLocators;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.webdriver.ExtendedElement;

/**
 * @author chandrasekhar.rao
 *
 */
public class TeacherHomePage extends BaseTestPage<TestPage> implements TeacherPageLocators {

	Logger log = Logger.getLogger(String.class);
	TestBase testBase = TestBaseProvider.getTestBase();

	@FindBy(locator = TEACHER_LOGO)
	private ExtendedElement teacherLogo;

	public ExtendedElement getTeacherLogo() {
		return teacherLogo;
	}

	@FindBy(locator = RESOURCES_TOOLS_TAB)
	private ExtendedElement resourcesAndToolsTab;

	public ExtendedElement getResourcesAndToolsTab() {
		return resourcesAndToolsTab;
	}

	@FindBy(locator = BOOKS_AUTHER)
	private ExtendedElement booksAndAuther;

	public ExtendedElement getBooksAndAuther() {
		return booksAndAuther;
	}

	public void verifyUserIsOnTeacherSite() {
		AssertUtils.assertDisplayed(getResourcesAndToolsTab());
	}

	public void verifyTeacherLogo() throws Exception {
		// ((JavascriptExecutor)getDriver()).executeScript("arguments[0].style.visibility='hidden'",
		// getTeacherLogo());
		if (!testBase.getContext().getString("applitools").equalsIgnoreCase("false")) {
			ApplitoolsUtils.getEyes().checkRegion(getTeacherLogo());
		}
	}

	public void checkLinksOnResourceAndToolsTab() throws Exception {
		Actions action = new Actions(getDriver());
		action.moveToElement(getResourcesAndToolsTab()).perform();
		if (!testBase.getContext().getString("applitools").equalsIgnoreCase("false")) {
			ApplitoolsUtils.getEyes().checkWindow("linksonresourceandtollstab");
		}
	}

	public void checkLinksOnBooksAndResourcesTab() throws Exception {
		/* ((JavascriptExecutor)getDriver()).executeScript("arguments[0].style.visibility='hidden'",
		 getBooksAndAuther());*/
		Actions action = new Actions(getDriver());
		action.moveToElement(getBooksAndAuther()).perform();
		if (!testBase.getContext().getString("applitools").equalsIgnoreCase("false")) {
			ApplitoolsUtils.getEyes().checkWindow("linksonbooksandauhertab");
		}
	}

	@Override
	protected void openPage() {
		getDriver().get(TestBaseProvider.getTestBase().getString("url"));
	}

	@Override
	public void launchPage() {
		openPage();
	}
}

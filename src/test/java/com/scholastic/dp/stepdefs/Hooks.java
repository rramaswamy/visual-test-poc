package com.scholastic.dp.stepdefs;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.applitools.Utilities.ApplitoolsUtils;
import com.applitools.eyes.Eyes;
import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.RectangleSize;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	
	    WebDriver driver = TestBaseProvider.getTestBase().getDriver();   
	    private static final String MY_MAP = "myMap";
	 	public static Eyes eyes;
	 	public static Map<String, Eyes> myMap =  new HashMap<String, Eyes>();

	/*@Before
	public void beforeHook(Scenario scenario) {
		synchronized (this) {
			TestBase testBase = TestBaseProvider.getTestBase();
			testBase.getContext().subset("testexecution").clear();
			String session = testBase.getSessionID();

			if (!session.equalsIgnoreCase("") && !testBase.getContext().getString("sauce")
					.equalsIgnoreCase("false")) {
				SauceREST sClient =
						new SauceREST(testBase.getContext().getString("sauce.username"),
								testBase.getContext().getString("sauce.access.key"));
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("name", scenario.getName());
				sClient.updateJobInfo(session, params);
			}
			
			if (!session.equalsIgnoreCase("") && !testBase.getContext().getString("applitools")
					.equalsIgnoreCase("false")) {
				eyes = new Eyes();
				eyes.setApiKey("9tHA7E3iDCCYDmKq9DGVL602Ei0Ahazd4BISa962UCU110");
				eyes.setBaselineName("Applitools Test on Sauce Lab");
				eyes.setMatchLevel(MatchLevel.STRICT);
				eyes.setForceFullPageScreenshot(true);
				eyes.setHideScrollbars(true);
				driver = eyes.open(testBase.getDriver(), "Digital Processing", "Applitools Test on Sauce Lab",new RectangleSize(1000, 600));
			}
			
			testBase.getDriver().manage().deleteAllCookies();
			testBase.getDriver().manage().window().maximize();
			testBase.getDriver().get(testBase.getContext().getString("url"));
		}
	}
	
	@After
	public void afterHook(Scenario scenario) {
		synchronized (this) {
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			if (scenario.isFailed()) {
				try {
					scenario.write("Current Page URL is " + driver.getCurrentUrl());
					byte[] screenshot =
							((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
					scenario.embed(screenshot, "image/png");
					
				} catch (WebDriverException somePlatformsDontSupportScreenshots) {
					System.err.println(somePlatformsDontSupportScreenshots.getMessage());
				}

			} 
			String session = TestBaseProvider.getTestBase().getSessionID();
			if (!scenario.isFailed())
				System.out.println("ScenarioFailed=" + scenario.getName() + "<>Session="
						+ session + "<>Status=Passed<>Platform=" + TestBaseProvider
								.getTestBase().getContext().getString("driver.name"));
			else
				System.out.println("ScenarioFailed=" + scenario.getName() + "<>Session="
						+ session + "<>Status=Failed<>Platform=" + TestBaseProvider
								.getTestBase().getContext().getString("driver.name"));
			System.out.println("SauceOnDemandSessionID=" + session + " job-name="
					+ scenario.getName());
			if (!session.equalsIgnoreCase("") && !TestBaseProvider.getTestBase()
					.getContext().getString("sauce").equalsIgnoreCase("false")) {
				TestBase testBase = TestBaseProvider.getTestBase();
				SauceREST sClient =
						new SauceREST(testBase.getContext().getString("sauce.username"),
								testBase.getContext().getString("sauce.access.key"));
				System.out.println("SessionID::" + session);
				if (scenario.isFailed())
					sClient.jobFailed(session);
				else sClient.jobPassed(session);
			}
			TestBaseProvider.getTestBase().tearDown();
		}
	}*/
	
	
	@Before
	/**
	 * Delete all cookies at the start of each scenario to avoid shared state
	 * between tests/	 */
	public void beforeHooks(Scenario scenario) throws Exception {
		
		String scenarioName = scenario.getName();
		TestBase testBase = TestBaseProvider.getTestBase();
		testBase.getContext().subset("testexecution").clear();
		
		if (!testBase.getContext().getString("applitools")
				.equalsIgnoreCase("false")) {
			eyes = new Eyes();
			eyes.setApiKey("QxcX9kF6N3unILshx7O7cGExcqbaLQg9eObjulgcx3A110");
			eyes.setBaselineName(scenarioName);
			eyes.setMatchLevel(MatchLevel.STRICT);
			eyes.setForceFullPageScreenshot(true);
			eyes.setHideScrollbars(true);
			driver = eyes.open(TestBaseProvider.getTestBase().getDriver(), "DP Testingssss",scenarioName,new RectangleSize(1000, 600));
			myMap.put(MY_MAP, eyes);	
		}	
		TestBaseProvider.getTestBase().getDriver().manage().deleteAllCookies();
		TestBaseProvider.getTestBase().getDriver().get("http://www.scholastic.com/teachers/");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
	}
	
	@After
	/**
	 * Embed a screenshot in test report if test is marked as failed
	 */
	public void embedScreenshot(Scenario scenario) {
		TestBase testBase = TestBaseProvider.getTestBase();
		
		if (!testBase.getContext().getString("applitools")
				.equalsIgnoreCase("false")) {
			ApplitoolsUtils.getEyes().close();
			
		}
		
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + driver.getCurrentUrl());
				// byte[] screenshot = getScreenshotAs(OutputType.BYTES);
				byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
			} catch (WebDriverException somePlatformsDontSupportScreenshots) {
				System.err.println(somePlatformsDontSupportScreenshots.getMessage());
			}
		}
		 System.getProperty("webdriver.chrome.driver");
		TestBaseProvider.getTestBase().getDriver().manage().deleteAllCookies();
		TestBaseProvider.getTestBase().tearDown();
	}

}
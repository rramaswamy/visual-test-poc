
package com.scholastic.dp.stepdefs;

import com.scholastic.dp.pageobjects.TeacherHomePage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author chandra.rao
 *
 */
public class UIValidationsStepdefs {
	
	@When("^verify user is on teacher site$")
	public void verify_teacher_site_page() throws Exception {
		TeacherHomePage teacherHomePage = new TeacherHomePage();
		teacherHomePage.verifyUserIsOnTeacherSite();		
	}
	
	@Then("^verify resources and tools tab is present$")
	public void verify_resources_tools_tab() throws Exception {
		TeacherHomePage teacherHomePage = new TeacherHomePage();
		teacherHomePage.verifyUserIsOnTeacherSite();		
	}
	
	@Then("^verify teacher logo is present by applitool$")
	public void verify_teacher_logo() throws Exception {
		TeacherHomePage teacherHomePage = new TeacherHomePage();
		teacherHomePage.verifyTeacherLogo();		
	}
	
	@Then("^verify links on resource and tools tab$")
	public void verify_links_on_resource_tools_tab() throws Exception {
		TeacherHomePage teacherHomePage = new TeacherHomePage();
		teacherHomePage.checkLinksOnResourceAndToolsTab();	
	}
	
	@Then("^verify links on books and auther tab$")
	public void verify_links_on_books_and_auther_tab() throws Exception {
		TeacherHomePage teacherHomePage = new TeacherHomePage();
		teacherHomePage.checkLinksOnBooksAndResourcesTab();	
	}
}

/**
 * 
 */
package com.scholastic.dp.tests;

/**
 * @author smitarani.bhol
 *	This class contains project level exceptions
 */
public class DPException extends Exception{
	public DPException(String sMessage){
		super(sMessage);
	}
	
}

package com.scholastic.dp.utils;

import java.io.File;
import java.net.URL;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;

import com.scholastic.torque.common.AssertUtils;

public class CsvUpload {

	private RemoteWebDriver driver;

	public void setUp() throws Exception {

		/*DesiredCapabilities capabillities = DesiredCapabilities.firefox();
		capabillities.setCapability("version", "40.0");
		capabillities.setCapability("platform", Platform.MAC);
		capabillities.setCapability("selenium-version", "2.48.2");*/

		
		  DesiredCapabilities capabillities = DesiredCapabilities.chrome();
		  capabillities.setCapability("version", "latest");
		  capabillities.setCapability("platform", Platform.MAC);
		 // capabillities.setCapability("selenium-version", "2.48.2");
		 
		capabillities.setCapability("name", "Remote File Upload using Selenium 2's FileDetectors");

		driver = new RemoteWebDriver(
				new URL("http://eCMS_qe:dedee5d0-469c-4c6b-a308-a36cc7413dbf@ondemand.saucelabs.com:80/wd/hub"),
				capabillities);
		//driver.setFileDetector(new LocalFileDetector());
	}

	public void testSauce() throws Exception {
		driver.get("https://dp-portal-dev1.scholastic-labs.io/#/signin/staff");
	    driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//input[@ng-model='user.username']")).sendKeys("auto.dpqa1@gmail.com");
		driver.findElement(By.id("loginPasswordInput")).sendKeys("Winter12");
		driver.findElement(By.xpath("//*[@id='loginButton']")).click();
		Thread.sleep(5000);
		
		driver.findElement(By.linkText("Students")).click();
		Thread.sleep(3000);
		
		RandomStringUtils random = new RandomStringUtils();
		String sClassName = "AutoClass" + random.randomAlphabetic(4);
		System.out.println("New class :" + sClassName);
		
		Thread.sleep(3000);
		
		driver.findElement(By.id("addClassLink")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.id("classTitleInput")).sendKeys(sClassName);
		
		driver.findElement(By.xpath("//*[@id='createClassLowGrade']/button")).click();
		driver.findElement(By.xpath(String.format("//a[contains(.,'Grade 3')]"))).click();

		driver.findElement(By.id("createupdateSubmitButton")).click();
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("//a[contains(.,'Import students with a CSV File')]")).click();
		Thread.sleep(3000);
		
		WebElement El = driver.findElement(By.xpath("//input[@id='csvUpload']"));
		
		///File upload code 1
		
		LocalFileDetector detector = new LocalFileDetector();
		String path = "/Users/is-4583/Projects/digital-platform-test-automation/DPTest/src/test/resources/samplecsv.csv";
		File f = detector.getLocalFile(path);
		((RemoteWebElement)El).setFileDetector(detector);
		El.sendKeys(f.getAbsolutePath());
		
		///File upload code 2
		
	/*	JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("document.getElementsByName('File Upload')[0].setAttribute('type', 'file');");
		
		((RemoteWebElement) El ).setFileDetector(new LocalFileDetector()); 
		El.sendKeys("/Users/is-4583/Projects/digital-platform-test-automation/DPTest/src/test/resources/samplecsv.csv");
		*/
		///File upload code 3
		//JavascriptExecutor jse = (JavascriptExecutor)driver;
		/*jse.executeScript("document.getElementByName('File Upload')[0].setAttribute('type', 'file');");
		WebElement upload = driver.findElement(By.xpath("//input[@id='csvUpload']"));    //("csvUpload"));
        upload.sendKeys("/Users/is-4583/Projects/digital-platform-test-automation/DPTest/src/test/resources/samplecsv.csv");
		*/
	}

	public WebElement findElementByText(String Text) {
		WebElement element = driver.findElement(By.xpath("//*[contains(text(),'" + Text + "')]"));
		return element;
	}

}

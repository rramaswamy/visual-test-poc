package com.scholastic.dp.utils;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestLocal {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://dp-portal-dev1.scholastic-labs.io/#/signin/staff");
	    driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//input[@ng-model='user.username']")).sendKeys("auto.dpqa1@gmail.com");
		driver.findElement(By.id("loginPasswordInput")).sendKeys("Winter12");
		driver.findElement(By.xpath("//*[@id='loginButton']")).click();
		Thread.sleep(5000);
		
		driver.findElement(By.linkText("Students")).click();
		Thread.sleep(3000);
		
		RandomStringUtils random = new RandomStringUtils();
		String sClassName = "AutoClass" + random.randomAlphabetic(4);
		System.out.println("New class :" + sClassName);
		
		Thread.sleep(3000);
		
		driver.findElement(By.id("addClassLink")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.id("classTitleInput")).sendKeys(sClassName);
		
		driver.findElement(By.xpath("//*[@id='createClassLowGrade']/button")).click();
		driver.findElement(By.xpath(String.format("//a[contains(.,'Grade 3')]"))).click();

		driver.findElement(By.id("createupdateSubmitButton")).click();
		
		driver.findElement(By.xpath("//a[contains(.,'Import students with a CSV File')]")).click();
	
	}

}

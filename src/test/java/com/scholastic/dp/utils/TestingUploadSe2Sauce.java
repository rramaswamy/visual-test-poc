package com.scholastic.dp.utils;

	
	import junit.framework.Assert;
	import junit.framework.TestCase;
	import org.openqa.selenium.*;
	import org.openqa.selenium.remote.*;
	import java.net.URL;
	import java.util.concurrent.TimeUnit;

	public class TestingUploadSe2Sauce  {
	    private RemoteWebDriver driver;
	   

	    public void setUp() throws Exception {
	    	
	        DesiredCapabilities capabillities = DesiredCapabilities.firefox();
	        capabillities.setCapability("version", "40.0");
	        capabillities.setCapability("platform", Platform.MAC);
	        capabillities.setCapability("selenium-version", "2.48.2");
	    	
	     /*   DesiredCapabilities capabillities = DesiredCapabilities.chrome();
	        capabillities.setCapability("version", "latest");
	        capabillities.setCapability("platform", Platform.MAC);
	        capabillities.setCapability("selenium-version", "2.48.2");*/
	        capabillities.setCapability("name", "Remote File Upload using Selenium 2's FileDetectors");

	        driver = new RemoteWebDriver(
	           new URL("http://eCMS_qe:dedee5d0-469c-4c6b-a308-a36cc7413dbf@ondemand.saucelabs.com:80/wd/hub"),
	           capabillities);
	        driver.setFileDetector(new LocalFileDetector());
	    }

	    public void testSauce() throws Exception {
	        driver.get("http://sl-test.herokuapp.com/guinea_pig/file_upload");
	        WebElement upload = driver.findElement(By.id("myfile"));
	        upload.sendKeys("/Users/is-4583/Projects/digital-platform-test-automation/DPTest/src/test/resources/samplecsv.csv");
	        driver.findElement(By.id("submit")).click();
	        Thread.sleep(5000);
	    }

	    public void tearDown() throws Exception {
	        driver.quit();
	    }
	}

package com.scholastic.dp.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebElement;
/**
 * @author chandrasekhar.rao
 *
 */

public class fileUtils {
	
	private static Robot robot;
	private static File file;
	private static StringSelection stringSelection;
	
	public static void fileUploadOnMac(String fileName) throws Exception {
		 
		// Place the file in resources folder
		//File Need to be imported 
		
		//http://ebpci-qa.sts.scholastic.com:8080/view/SDM/job/SDM-UI-Validations-Suite/ws/DPTest/src/test/resources/
		 file = new File(System.getProperty("user.dir")+"/src/test/resources/"+fileName); 
		//file = new File("http://ebpci-qa.sts.scholastic.com:8080/view/SDM/job/SDM-Regression-Suite/ws/DPTest/src/test/resources/"+fileName); 
		// http://ebpci-qa.sts.scholastic.com:8080/view/SDM/job/SDM-Regression-Suite/ws/DPTest/src/test/resources/
		 stringSelection = new StringSelection(file.getAbsolutePath());
		 
		//Copy to clipboard 
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		  
		// Cmd + Tab is needed since it launches a Java app and the browser looses focus
		robot = new Robot();
		 
		robot.keyPress(KeyEvent.VK_META);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_META);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.delay(500);
		 
		//Open Goto window
		 
		robot.keyPress(KeyEvent.VK_META);
		robot.keyPress(KeyEvent.VK_SHIFT);
		robot.keyPress(KeyEvent.VK_G);
		robot.keyRelease(KeyEvent.VK_META);
		robot.keyRelease(KeyEvent.VK_SHIFT);
		robot.keyRelease(KeyEvent.VK_G);
		 
		//Paste the clipboard value
		robot.keyPress(KeyEvent.VK_DELETE);
		robot.keyPress(KeyEvent.VK_META);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_META);
		robot.keyRelease(KeyEvent.VK_V);
		
		//Press Enter key to close the Goto window
		robot.delay(500); 
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.delay(500);
		
		// Double click on file
		robot.mousePress(InputEvent.BUTTON1_MASK ); 
		robot.mouseRelease(InputEvent.BUTTON1_MASK );
		
		// Close upload window
		robot.keyPress(KeyEvent.VK_ENTER); 
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);	
	}
	
	public static void fileUploadOnWindows(String fileName) throws Exception {
		
		// Place the file in resources folder
		//File Need to be imported 
		 file = new File(System.getProperty("user.dir")+"/src/test/resources/"+fileName); 
		 stringSelection= new StringSelection(file.getAbsolutePath());
		 
		//Copy to clipboard 
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

		//imitate events like ENTER, CTRL+C, CTRL+V
		robot = new Robot();
		robot.delay(250);		
	    robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
	}
	
	public static void uploadFileToSauceLab(String fileName, WebElement inputElement,String absoluteFilepath) {
		
       // WebElement csvInputElement = getDriver().findElement(By.xpath("//input[@id='csvUpload']"));
		
		// File path in workspace in Local machine
		//String path = "/Users/is-4583/Projects/digital-platform-test-automation/DPTest/src/test/resources/samplecsv.csv";
		// File path in workspace in Jenkins master
		String filepath = absoluteFilepath+fileName;
		
		LocalFileDetector detector = new LocalFileDetector();
		File f = detector.getLocalFile(filepath);
		((RemoteWebElement)inputElement).setFileDetector(detector);
		inputElement.sendKeys(f.getAbsolutePath());
		
	}
	
}
